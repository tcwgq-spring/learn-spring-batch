package com.tcwgq.learn_spring_batch.batch;

import com.tcwgq.learn_spring_batch.bean.Article;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MyItemProcessor implements ItemProcessor<Article, Article> {
    @Override
    public Article process(Article article) throws Exception {
        log.info("【{}】经过处理器", article.getTitle());
        return article;
    }
}
