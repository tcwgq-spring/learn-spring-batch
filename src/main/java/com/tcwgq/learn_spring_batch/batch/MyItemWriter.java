package com.tcwgq.learn_spring_batch.batch;

import com.tcwgq.learn_spring_batch.bean.Article;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

@Slf4j
public class MyItemWriter implements ItemWriter<Article> {
    @Override
    public void write(List<? extends Article> items) throws Exception {
        items.forEach(article -> log.info("【{}】写入数据库", article.getTitle()));
    }
}
