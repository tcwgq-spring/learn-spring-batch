package com.tcwgq.learn_spring_batch.bean;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_article")
public class Article {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String author;
    private String title;
    private String property;
    private String picture;
    private String keyword;
    private String content;
    private String description;
    private String source;
    private Long click;

}
