package com.tcwgq.learn_spring_batch.config;

import com.tcwgq.learn_spring_batch.batch.MyItemProcessor;
import com.tcwgq.learn_spring_batch.batch.MyItemWriter;
import com.tcwgq.learn_spring_batch.bean.Article;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.integration.partition.BeanFactoryStepLocator;
import org.springframework.batch.integration.partition.StepExecutionRequestHandler;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.orm.JpaNativeQueryProvider;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;

import javax.persistence.EntityManagerFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class SlaveConfiguration {
    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Bean
    @ServiceActivator(inputChannel = "inboundRequests", outputChannel = "outboundStaging")
    public StepExecutionRequestHandler stepExecutionRequestHandler() {
        StepExecutionRequestHandler stepExecutionRequestHandler = new StepExecutionRequestHandler();
        BeanFactoryStepLocator stepLocator = new BeanFactoryStepLocator();
        stepLocator.setBeanFactory(applicationContext);
        stepExecutionRequestHandler.setStepLocator(stepLocator);
        stepExecutionRequestHandler.setJobExplorer(jobExplorer);
        return stepExecutionRequestHandler;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Article> reader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue) {
        log.info("接收到分片参数[" + minValue + "->" + maxValue + "]");
        JpaPagingItemReader<Article> reader = new JpaPagingItemReader<>();
        reader.setEntityManagerFactory(entityManagerFactory);
        // provider
        JpaNativeQueryProvider<Article> provider = new JpaNativeQueryProvider<>();
        provider.setSqlQuery("select * from t_article where id >= :minValue and id <= :maxValue");
        provider.setEntityClass(Article.class);
        reader.setQueryProvider(provider);
        // params
        Map<String, Object> parameterValues = new HashMap<>(16);
        parameterValues.put("minValue", minValue);
        parameterValues.put("maxValue", maxValue);
        reader.setParameterValues(parameterValues);
        return reader;
    }

    @Bean("slaveStep")
    public Step slaveStep(MyItemProcessor processorItem, JpaPagingItemReader<Article> reader) {
        CompositeItemProcessor<Article, Article> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(Collections.singletonList(processorItem));
        return stepBuilderFactory.get("slaveStep")
                // 事务提交批次
                .<Article, Article>chunk(1000)
                .reader(reader)
                .processor(compositeItemProcessor)
                .writer(new MyItemWriter())
                .build();
    }

}
