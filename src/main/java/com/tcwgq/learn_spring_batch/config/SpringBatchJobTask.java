package com.tcwgq.learn_spring_batch.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Component
public class SpringBatchJobTask {
    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    JobOperator jobOperator;

    @Resource(name = "masterJob")
    private Job batchJob;

    @Scheduled(cron = "*/5 * * * * ?")
    public void schedule() throws Exception {
        log.info("SpringBatchJobTask schedule...");
    }

    public void task() throws Exception {
        log.info("spring job task executing...");
        JobParameters jobParameter = new JobParametersBuilder()
                .addLong("time", System.currentTimeMillis())
                .addString("name", "zhangSan")
                .addDate("date", new Date())
                .toJobParameters();
        JobExecution run = jobLauncher.run(batchJob, jobParameter);
        log.info("run status {}", run);
    }

}