CREATE TABLE `t_article`
(
    `id`          bigint       NOT NULL AUTO_INCREMENT,
    `author`      varchar(100) NOT NULL,
    `title`       varchar(255)      DEFAULT NULL,
    `property`    varchar(255)      DEFAULT NULL,
    `picture`     varchar(255)      DEFAULT NULL,
    `keyword`     varchar(255)      DEFAULT NULL,
    `content`     varchar(255)      DEFAULT NULL,
    `description` varchar(255)      DEFAULT NULL,
    `source`      varchar(255)      DEFAULT NULL,
    `click`       bigint            DEFAULT NULL,
    `create_dt`   timestamp    NULL DEFAULT NULL,
    `update_dt`   timestamp    NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = MyISAM
  AUTO_INCREMENT = 21
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci